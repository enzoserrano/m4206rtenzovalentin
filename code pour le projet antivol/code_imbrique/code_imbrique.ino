const int TouchPin=2;
const int ledPin=3;
const int pinAdc = A0;

void setup() {
    Serial.begin(115200);
    pinMode(TouchPin, INPUT);
    pinMode(ledPin,OUTPUT);
      
  //Serial.println("Grove - Sound Sensor Test...");
    pinMode(6, OUTPUT); //port de sortie du code
}

void loop() {
  capteurTouche();
  graphSonor();
  
}

void graphSonor() {
  long sum = 0;
  for (int i = 0; i < 32; i++)
  {
    sum += analogRead(pinAdc);
  }

  sum >>= 5;

  Serial.println(sum);
  delay(10);
   
}

void capteurTouche(){
    int sensorValue = digitalRead(TouchPin);
    if(sensorValue==1)
    {
        digitalWrite(ledPin,HIGH);
    }
    else
    {
        digitalWrite(ledPin,LOW);
    }
  
}
